from django.contrib import admin

# Register your models here.


from .models import Friendship, Circles, Logs, UserProfile, tinylog



admin.site.register(Friendship)
admin.site.register(Circles)
admin.site.register(Logs)
admin.site.register(UserProfile)
admin.site.register(tinylog)