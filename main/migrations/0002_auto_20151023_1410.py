# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='circles',
            name='description',
            field=models.TextField(help_text=b'Describe your group...', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='circles',
            name='name',
            field=models.CharField(default=b' ', help_text=b'Name your group!', max_length=50),
        ),
    ]
