from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import User
from django.db.models import Sum
from main.models import *
from BC.models import *

from BC.forms import *
# Create your views here.

#function that will get notifications from any user
def notify(logged_user):
	return Notification.objects.filter(owner=logged_user)
	
@login_required
def index(request):
	notifications = notify(request.user) #get notifications

	return HttpResponseRedirect('/logs/')

@login_required
def search(request):
	notifications = notify(request.user)
	if 'q' in request.GET: #if a search has been done
		template = 'BC/search.html'
		result = []
		query = request.GET['q'].strip() #remove whitespace on the begining or the end of the string
		if (' ' in request.GET['q']) == True: #if there is a space in the string e.g. 'chino y nacho' will return True
			first_name = query.partition(' ')[0] #gets 'chino' from string
			last_name = query.split(' ',1)[1:][0]#gets 'y nacho' from string

			#assuming that the name exists, will proceed doing the search 
			#First, we'll check if the exact name exists. If True, then we have found our user! CASE INSENSITIVE
			complete_user = User.objects.filter(first_name__iexact=first_name, last_name__iexact=last_name)
			if complete_user:
				result.extend(list(complete_user))
				instance = RequestContext(request, locals())
				return render_to_response(template, context_instance=instance)
			#Else, we'll find any coincidences with them names. CASE INSENSITIVE
			complete_user = User.objects.filter(first_name__icontains=first_name, last_name__icontains=last_name)
			first_name_filter = User.objects.filter(first_name__icontains=first_name)
			last_name_filter = User.objects.filter(last_name__icontains=last_name)

			result.extend(list(complete_user))
			result.extend(list(first_name_filter))
			result.extend(list(last_name_filter))
			
			result = set(result)

			instance = RequestContext(request, locals())
			return render_to_response(template, context_instance=instance)
		
		#Username could be unique. So let's check if the exact string exists. If True, we have found our user! CASE INSENSITIVE
		username_filter = User.objects.filter(username__iexact=request.GET['q'])
		if username_filter:
			result.extend(list(username_filter))
			instance = RequestContext(request, locals())
			return render_to_response(template, context_instance=instance)

		#Else, we'll find any match CASE INSENSITIVE
		username_filter = User.objects.filter(username__icontains=request.GET['q'])

		#Now we find any coincidence with the fist_name or last_name param CASE INSENSITIVE
		first_name_filter = User.objects.filter(first_name__icontains=request.GET['q'])
		last_name_filter = User.objects.filter(last_name__icontains=request.GET['q'])
		
		result.extend(list(username_filter))
		result.extend(list(first_name_filter))
		result.extend(list(last_name_filter))

		result = set(result)

		instance = RequestContext(request, locals())
		return render_to_response(template, context_instance=instance)
	return HttpResponseRedirect('/')

@login_required
def profile(request, username):
	notifications = notify(request.user)
	#first, check if user exists. Else, redirect to 'search'
	try:
		user_friend = User.objects.get(username=username)
	except User.DoesNotExist:
		return HttpResponseRedirect('/search?q='+username)


	#Now, we got the request.user and the user_friend. We'll try and check if the User_friend added the request.user as a friend
	friend_request = Friendship.objects.filter(creator=user_friend,friend=request.user)
	if friend_request:
		friend_request = friend_request[0]
		#Ok! There's a friendship, now let's check if the status is Pending
		if friend_request.status == 'P':
			#Set a flag true! To render the buttons of "Accept" or "Decline"
			friend_request_flag = True
		
	#now, we check if there's a friendship. If there is, then we load the user profile, if not, the option to add friend will be available
	friendship = Friendship.objects.filter(creator=request.user,friend=user_friend)
	if friendship:
		friendship = friendship[0]

	#if theres and 'add' then we should add a new friend
	if 'add' in request.GET:
		if request.GET['add'] == 'true':
			if not user_friend == request.user:
				#If there is a existing Friend Request, we update the status
				try:
					friend_request = Friendship.objects.get(creator=user_friend,friend=request.user)
					#So it means that we have a status = N. Let's update it to 'P'
					friend_request.friend_request_sent()
				except Friendship.DoesNotExist:					
					#else, create record for friendship. Here, we have a status of pending.
					f1_request = Friendship(creator=request.user, friend=user_friend, status='P')
					f1_request.save()
					#we must now create a notification to the user_friend, so he can accept the friend request
					content = 'Hi there! Please add me as your friend! - ' + request.user.username
					notification = Notification(owner=user_friend, from_notification=request.user,content=content)
					notification.save()
			return HttpResponseRedirect('/profile/'+username)

	#let's unfriend!
	if 'unfriend' in request.GET:
		if not user_friend == request.user:
			#let's make sure the status is 'A' so we can unfriend
			if friend_request.status == 'A':
				#now we unfriend
				friend_request.delete()
				friendship.delete()
		return HttpResponseRedirect('/profile/'+username)

	#If there is a Accept or Decline in a friendship
	if 'accept' in request.GET or 'decline' in request.GET:
		#Let's make sure that there is, in fact, a friend request to accept or decline
		if friend_request:
			#Now let's make sure that the status is Pending
			if friend_request_flag:
				f = Notification.objects.filter(owner=request.user, from_notification=user_friend)
				
				#now, we'll handle the decline. IF decline, we'll just update the status to N
				if 'decline' in request.GET:
					friend_request.unfriend()
					#If there is the other way friendship, we need to set to 'N' its status. If not, then do nothing else
					if friendship:
						friendship.unfriend()
					if f:
						f = f[0]
						f.delete()
				#IF accept, we'll update friend_request to 'A' AND add the other way relationship so now user X is friend of Y AND user Y is friend of X
				elif 'accept' in request.GET:
					friend_request.friend_request_accepted()
					if f:
						f = f[0]
						f.delete()
					#add new friendship
					new_friendship = Friendship(creator=request.user,friend=user_friend,status='A')
					new_friendship.save()
				return HttpResponseRedirect('/profile/'+username)

	template = 'BC/profile.html'
	instance = RequestContext(request, locals())
	return render_to_response(template, context_instance=instance)

@login_required
def circles(request):
	notifications = notify(request.user)
	my_circles = []
	# we must know search all circles from a request.user
	my_friendships = Friendship.objects.filter(creator=request.user)
	if my_friendships:
		#for friendship in my_friendships:
		my_circles.extend(list(Circles.objects.filter(group=my_friendships).order_by('-pk')))

	#perhaps, he didn't create the group?
	my_friendships = Friendship.objects.filter(friend=request.user)
	if my_friendships:
		#for friendship in my_friendships:
		my_circles.extend(list(Circles.objects.filter(group=my_friendships).order_by('-pk')))

	print my_circles
	my_circles = set(my_circles)

	#return HttpResponse(my_circles)

	template = 'BC/circles.html'
	instance = RequestContext(request, locals())
	return render_to_response(template, context_instance=instance)

@login_required
def circle_create(request):
	notifications = notify(request.user)
	#get all friends of the request.user
	friends = Friendship.objects.filter(creator=request.user, status='A')

	if 'save' in request.POST:
		values = request.POST.getlist('group')
		new_circle = Circles(name=request.POST['name'], description=request.POST['description'])
		new_circle.save()
		print new_circle.pk
		for item in values:
			i = Friendship.objects.filter(pk=item)
			if i:
				new_circle.group.add(i[0])
		
		return HttpResponseRedirect('/logs/')
		

	template = 'BC/circle_add.html'
	instance = RequestContext(request, locals())
	return render_to_response(template, context_instance=instance)
	#TODO - puedes agregar, pero no puedes modificar ni buscar

@login_required
def logs(request, id):
	notifications = notify(request.user)
	log_list = Logs.objects.filter(group=id).order_by('description') #list of log for the current group. ESTE ES MI OBJETO PRINCIPAL
	friendships = Circles.objects.filter(pk=id) #current group information. needed to access friends in the group.
	current_id = id;
	d = {} #values for log 
	a = [] #to build the next url. We'll save here the pk of each logs
	b = []
	counter = 0
	if friendships: 
		friendships = friendships[0]
		users = util(friendships)
		for obj in log_list:
			l = {} #values for users
			items = tinylog.objects.filter(log=obj)
			a.append(items[0].pk)
			print log_list[counter].description
			b.append(log_list[counter].description)
			counter += 1
			for user in users:
				aux = items.filter(who=user)
				l[user] = aux.aggregate(Sum('value'))['value__sum']
			l['total'] = items.aggregate(Sum('value'))['value__sum']
			d[obj.description] = l

	
	template = 'BC/logs.html'
	instance = RequestContext(request, locals())
	return render_to_response(template, context_instance=instance)

@login_required
def log_detail(request, pid, id):
	notifications = notify(request.user)
	curr1 = pid
	curr2 = id
	log_list = Logs.objects.filter(pk=id)	
	if log_list:
		log_list = log_list[0]
	tinylog_list = tinylog.objects.filter(log=log_list)

	template='BC/history_detail_log.html'
	instance = RequestContext(request, locals())
	return render_to_response(template, context_instance=instance)

def log_detail_add(request, pid, id):
	notifications = notify(request.user)
	curr1 = pid
	curr2 = id
	log_list = Logs.objects.filter(pk=id)	
	if log_list:
		log_list = log_list[0]
	if 'save' in request.POST:
		tinyform = TinyLogForm(request.POST)
		if tinyform.is_valid():
			s = tinyform.save(commit=False)
			s.log = log_list
			s.save()
			return HttpResponseRedirect('/logs/'+pid+'/detail/'+id)
	else:
		tinyform = TinyLogForm()

	template = 'BC/anotherone.html'
	instance = RequestContext(request, locals())
	return render_to_response(template, context_instance=instance)
				
def util(circle):
	#necesito regresar, dado un Circle, los participantes de ese circle.
	d = []
	for element in circle.group.all():
		d.append(element.creator)
		d.append(element.friend)
	return list(set(d))

@login_required
def new_log(request, id):
	notifications = notify(request.user)
	group = Circles.objects.filter(pk=id)
	if group:
		group = group[0]
	current_id = id
	if 'save' in request.POST:
		tinylogform = TinyLogForm(request.POST)
		logform = LogForm(request.POST)
		if tinylogform.is_valid() and logform.is_valid():
			f = logform.save(commit=False)
			f.group = group
			f.save()
			
			t = tinylogform.save(commit=False)
			t.log = f
			t.save()

			return HttpResponseRedirect('/logs/'+current_id)
	else:
		tinylogform = TinyLogForm()
		logform = LogForm()
	template = 'BC/newlog.html'
	instance = RequestContext(request, locals())
	return render_to_response(template, context_instance=instance)
